
public class TennisGame2 implements TennisGame
{
    private static final int _FORTY = 3;
	private static final int _THIRTY = 2;
	private static final int _FIFTEEN = 1;
	private static final int _LOVE = 0;
	public int Player1PointGlobal = 0;
    public int Player2PointGlobal = 0;
    
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){
        String score = "";
        score = Normal2();
		if (IsTie())
            score =  getLiteral(Player1PointGlobal)+"-All";
		if (IsDuece())
            score = "Deuce";
		if (IsAvantage(Player1PointGlobal,Player2PointGlobal))
            score = "Advantage player1";
        if (IsAvantage(Player2PointGlobal,Player1PointGlobal))
            score = "Advantage player2";
		if (IsWinner(Player1PointGlobal, Player2PointGlobal))
            score = "Win for player1";
        if (IsWinner(Player2PointGlobal,Player1PointGlobal))
            score = "Win for player2";
        return score;
    }
	private String Normal2() {
		String score = "";
		if (IsNormal())
        {
			score = getResult();
        }
		return score;
	}

	private boolean IsNormal() {
		return Player1PointGlobal!=Player2PointGlobal;
	}

	private String getResult() {
		String score,Player1Point,Player2Point;
		Player1Point = getLiteral(Player1PointGlobal);
		Player2Point = getLiteral(Player2PointGlobal);
		score = Player1Point + "-" + Player2Point;
		return score;
	}

	private String getLiteral(int PlayerPoint) {
		String result = "";
		if (PlayerPoint==_LOVE)
			result = "Love";
		if (PlayerPoint==_FIFTEEN)
			result = "Fifteen";
		if (PlayerPoint==_THIRTY)
			result = "Thirty";
		if (PlayerPoint==_FORTY)
			result = "Forty";
		return result;
	}
	
	private boolean IsAvantage(int Player1Point,int Player2Point) {
		return Player1Point > Player2Point && Player2Point >= 3;
	}

	private boolean IsTie() {
		return Player1PointGlobal == Player2PointGlobal && Player1PointGlobal < 4;
	}

	private boolean IsWinner(int Player1Point , int Player2Point) {
		return Player1Point>=4 && Player2Point>=0 && (Player1Point-Player2Point)>=2;
	}

	private boolean IsDuece() {
		return Player1PointGlobal==Player2PointGlobal && Player1PointGlobal>=3;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        Player1PointGlobal++;
    }
    
    public void P2Score(){
        Player2PointGlobal++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}